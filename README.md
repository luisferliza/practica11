# Laboratorio Software Avanzado - Práctica 11

## Luis Fernando Lizama - 201602656

Los Archivos de docker file y docker compose se encuentran en su carpeta correspondiente dentro de /src

## Configuración general
El Script para eliminar las imagenes anteriores y crear las nuevas se encuentra en el archivo src/main.sh. Para ejecutarlo unicamente escribimos el comando
```bash
./main.sh
```

Para levantar toda la arquitectura con docker compose ejecutamos el comando 
```cmd
sudo docker-compose up
```

Y para detenerlo ejecutamos 
```cmd
sudo docker-compose down
```


 ## API REST
 Todas las API'S desarrolladas funcionan sobre NODEJS por lo que el docker file es el mismo para todas y se muestra a continuacion

Dockerfile:
```Dockerfile
FROM node
WORKDIR /App
ADD . /App
RUN npm install
ENV PORT 3000
ENV IP "192.168.0.0"
CMD ["node","index.js"]
```

## Nginx
```conf
include /etc/nginx/modules-enabled/*.conf;

http {
	# Proxy
         server {
            listen 80;
            server_name localhost 127.0.0.1;
            location / {
              root   /usr/share/nginx/html;
              index  index.html index.html;
            }

            location /repartidor {
                proxy_pass          http://driver:3200/;
                proxy_set_header    X-Forwarded-For $remote_addr;
            }

            location /esb {
                proxy_pass          http://ebs:5000/;
                proxy_set_header    X-Forwarded-For $remote_addr;
            }           

            location /restaurant {
                proxy_pass          http://cliente:3000/;
                proxy_set_header    X-Forwarded-For $remote_addr;
            }
        }
}
```
 ## Docker Compose
El archivoo de docker compose se compone de 3 servicios y una red interna que permite la comunicación entre todos los servicios. Cada uno de los servicios posee una IP interna estática y los parámetros de conexión tambien se pasan por medio del docker compose. 
Dockerfile:
```yml
version: "3"
services:
  driver:
    container_name: driver
    image: driver
    ports:
      - "3200:3200"
    networks:
      - testing_net
            
    environment:
      - PORT=3200      
  
  restaurant:
    container_name: restaurant
    image: restaurant
    ports:
      - "3000:3000"
    environment:
      - EBSIP=ebs
    networks:
      - testing_net
             
    environment:
      - PORT=3000      

  ebs:
    container_name: ebs
    image: ebs
    ports:
      - "5000:5000"
    networks:
      - testing_net
            
    environment:
      - PORT=5000  
      - RESIP=restaurant
      - DRIIP=driver
  
  client:
    container_name: client
    image: client    
    networks:
      - testing_net            
    environment:
      - EBSIP=ebs
    depends_on:
      - ebs
    
  nginxProxy:
    image: nginx:1.17.10
    depends_on:
        - client
        - ebs
        - restaurant
        - driver
    volumes:
      - ./reverse_proxy/nginx.conf:/etc/nginx/nginx.conf:ro
    ports:
      - 80:80
    
networks:
  testing_net:
```